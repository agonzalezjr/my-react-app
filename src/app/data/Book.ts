import { Country } from "./Country";

export default class Book {
  constructor(public id: number,
              public title: string, 
              public description: string,
              public countries: Country[]) {}
}