import Book from "./Book";

import { List } from "immutable"
export default class BookFeed {

  private _lastId = 1000;
  private _books: List<Book>

  constructor(books: List<Book>) {
    this._books = List(books)
  }

  public get books() : List<Book> {
    return this._books
  }

  createBook(title: string, description: string) {
    // this._books = [new Book(this._lastId++, title, description, []), ...this._books]
    this._books = this._books.insert(0, new Book(this._lastId++, title, description, []));
  }

  updateBook(updatedBook: Book) {
    this._books = this._books.map(book => {
      if (book.id === updatedBook.id) {
        book.description = updatedBook.description
      }
      return book;
    })
  }

  deleteBook(deletedBook: Book) {
    // Replacing this (even throwing everything out) doesn't
    // trigger react when its hook is set to watch the whole
    // feed object :(
    // this._books = []
    this._books = this._books.filter(book => {
      return book.id !== deletedBook.id
    })
  }
}