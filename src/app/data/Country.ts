
export interface ICountry {
  name: string,
  icon: string
}

export class Country implements ICountry {
  constructor(public name: string, public icon: string) {}
}