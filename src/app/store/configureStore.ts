import { createStore, Store, applyMiddleware } from "redux"
import { testReducer, IAppState } from "../features/testarea/testReducer"
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

export const configureStore = (): Store<IAppState> => {
  const middlewares = [thunk]

  // this includes "devToolsEnhancer" already
  const composedEnhancer = composeWithDevTools(applyMiddleware(...middlewares))

  const store = createStore(testReducer, composedEnhancer)

  return store
}