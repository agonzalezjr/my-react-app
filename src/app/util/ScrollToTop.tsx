import { withRouter, RouteComponentProps } from "react-router";
import { useEffect, PropsWithChildren } from "react";

const ScrollToTop: React.FunctionComponent<PropsWithChildren<any> & RouteComponentProps> = ({ children, location: { pathname } }) => {

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return children || null;
};

export default withRouter(ScrollToTop);