import React from 'react'
import { Button, Icon } from 'semantic-ui-react';
import { RouteComponentProps } from 'react-router';

function HomePage({history}: RouteComponentProps) {
  return (
    <div>
      <h1>Eventually display the Atlas <span role="img" aria-label="map">🗺️</span></h1>
      <h2>For now see the list of books ...</h2>
      <Button size="huge" primary onClick={() => {history.push("/books")}}>
        Go there
        <Icon name="arrow right"></Icon>
      </Button>
    </div>
  )
}

export default HomePage
