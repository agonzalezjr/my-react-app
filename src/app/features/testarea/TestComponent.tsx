import React, { Component } from 'react'
import { connect } from 'react-redux';
import { IAppState, Actions, Action } from './testReducer';
import { Button } from 'semantic-ui-react';

interface ITestComponentStateProps {
  data: number
  flag: boolean
  loading: boolean,
  loadingElementName: string | null
}

// This method maps "store state" that this component
// cares about to "props" that this component can use
const mapStoreStateToProps = (state: IAppState): ITestComponentStateProps => ({
  data: state.data,
  flag: state.anObject.hidden,
  loading: state.loading,
  loadingElementName: state.elementName
})

interface ITestComponentActionProps {
  incrementData: Action,
  decrementData: Action,
  toggleFlag: Action,
  incrementAsync: any, // #andytodo: fix these types!
  decrementAsync: any,
}

// This object maps "reducer actions" that this component
// cares about to "props" that this component can use
const mapReducerActionsToProps: ITestComponentActionProps = Actions

// We used the same names, but the object could do custom mapping like so:
// const mapReducerActionsToProps: ITestComponentActionProps = {
//   incrementData: Actions.incrementData,
//   decrementData: Actions.decrementData
// }

// See how the types of the props are the return type of the map function ;)
class TestComponent extends Component<ITestComponentStateProps & ITestComponentActionProps> {
  render() {

    // destructure the props that come from mapStoreStateToProps
    const { data, flag, loading, loadingElementName } = this.props
    
    // destructure the props that come from mapReducerActionsToProps
    const { incrementData, decrementData, toggleFlag, incrementAsync, decrementAsync } = this.props

    return (
      <div>
        <h1>Test component</h1>
        <h3>The answer is: {data}</h3>
        <Button onClick={incrementData} positive content="++"></Button>
        <Button onClick={decrementData} negative content="--"></Button>
        <h3>The nested flag is: {String(flag)}</h3>
        <Button onClick={toggleFlag} active content="Toggle" color={flag ? "red" : "green"}></Button>
        <h3>The modal is: Closed</h3>
        <Button active content="Modal Test" color="teal"></Button>
        <h3>Async/Redux-thunk</h3>
        <Button 
          name="increment" 
          disabled={loading} 
          loading={"increment" === loadingElementName && loading} 
          onClick={(e: any) => incrementAsync(e.target.name)} 
          positive 
          content="++ (Async)">
        </Button>
        <Button 
          name="decrement" 
          disabled={loading} 
          loading={"decrement" === loadingElementName && loading} 
          onClick={(e: any) => decrementAsync(e.target.name)} 
          negative 
          content="-- (Async)">
        </Button>        
      </div>
    )
  }
}

// This wraps the component in a higher order redux component,
// and behind the scenes it's injecting the props into it.

// Also, when the functions in the actions mapping are called,
// the reducer is invoked with the right parameters.
export default connect(mapStoreStateToProps, mapReducerActionsToProps)(TestComponent)