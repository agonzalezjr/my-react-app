import Book from "../../data/Book";

// -------- ACTIONS ---------
export interface IReduxAction {
  type: string
  payload?: any
}

export type Action = (payload?: any) => IReduxAction

enum ActionTypes {
  INCREMENT_DATA = "INCREMENT_DATA",
  DECREMENT_DATA = "DECREMENT_DATA",
  TOGGLE_FLAG = "TOGGLE_FLAG",

  CREATE_BOOK = "CREATE_BOOK",
  UPDATE_BOOK = "UPDATE_BOOK",
  DELETE_BOOK = "DELETE_BOOK",

  ADD_COUNTRY = "ADD_COUNTRY",

  ASYNC_ACTION_START = "ASYNC_ACTION_START",
  ASYNC_ACTION_FINISH = "ASYNC_ACTION_FINISH",
  ASYNC_ACTION_ERROR = "ASYNC_ACTION_ERROR",
}

// Action Creator Helper
function createAction(type: string, payload?: any): IReduxAction {
  return payload === undefined ? { type } : { type, payload }
}

// Action Creators
export const Actions = {
  // Test Component Actions
  incrementData:() => createAction(ActionTypes.INCREMENT_DATA),
  decrementData:() => createAction(ActionTypes.DECREMENT_DATA),
  toggleFlag:() => createAction(ActionTypes.TOGGLE_FLAG),

  // Books Component Actions
  createBook:(book: Book) => createAction(ActionTypes.CREATE_BOOK, { book }),
  updateBook:(book: Book) => createAction(ActionTypes.UPDATE_BOOK, { book }),
  deleteBook:(book: Book) => createAction(ActionTypes.DELETE_BOOK, { book }),

  addCountry:(book: Book) => createAction(ActionTypes.ADD_COUNTRY, { book }),

  // Async
  asyncActionStart:(elementName: string) => createAction(ActionTypes.ASYNC_ACTION_START, { elementName }),
  asyncActionFinish:(elementName: string) => createAction(ActionTypes.ASYNC_ACTION_FINISH, { elementName }),
  asyncActionError:(elementName: string) => createAction(ActionTypes.ASYNC_ACTION_ERROR, { elementName }),
  incrementAsync:(elementName: string) => {
    return async (dispatch: any) => {
      dispatch(Actions.asyncActionStart(elementName))
      await delay(2000)
      dispatch(Actions.incrementData())
      dispatch(Actions.asyncActionFinish(elementName))
    }
  },
  decrementAsync:(elementName: string) => {
    return async (dispatch: any) => {
      dispatch(Actions.asyncActionStart(elementName))
      await delay(2000)
      dispatch(Actions.decrementData())
      dispatch(Actions.asyncActionFinish(elementName))
    }
  },
}

// --------- STATE ----------

// The state for the entire app: "root" state
export interface IAppState {
  // Test Component
  data: number,
  someMore: string,
  anObject: any

  // Book Components
  // Keeping constructable objects here, is causing
  // redux to cause react to not re-render. Let's try
  // with serialized POJOs ...
  books: any[],

  // Async
  loading: boolean,
  elementName: string | null,
}

// An initial state to start with
const initialState: IAppState = {
  // Test Component
  data: 42,
  someMore: "foo",
  anObject: {
    hidden: true,
    toggleCount: 0
  },

  // Book Components
  books: [
    {
      id: 1,
      title: "Turn Right at Machu Picchu",
      description: "Lorem ipsum bacon delicioso mucho rico entonces frgrance caipirinha puppy sumo horse some more gibberish",
      countries: [
        {
          name: "Peru",
          icon: "🇵🇪"
        }
      ]
    },
    {
      id: 2,
      title: "Bread is Awesome",
      description: "Lorem ipsum bacon delicioso mucho rico entonces frgrance caipirinha puppy sumo horse some more gibberish",
      countries: [
        {
          name: "France",
          icon: "🇫🇷"
        }
      ]
    },
  ],

  // Async
  loading: false,
  elementName: null
}

// -------- REDUCER --------

export const testReducer = (state: IAppState = initialState, action: IReduxAction): IAppState => {
  switch (action.type) {
    // Test Component
    case ActionTypes.INCREMENT_DATA:
      // A whole *NEW* state object is cloned/created!
      return {...state, data: state.data + 1}
    case ActionTypes.DECREMENT_DATA:
      return {...state, data: state.data - 1}
    case ActionTypes.TOGGLE_FLAG:
      return {
        ...state, 
        anObject: {
          ...state.anObject, 
          hidden: !state.anObject.hidden,
          toggleCount: state.anObject.toggleCount + 1,
        }
      }
  
    // Book Components
    case ActionTypes.CREATE_BOOK:
      const newBook = action.payload.book
      return {
        ...state,
        books: [newBook, ...state.books]
      }
    case ActionTypes.UPDATE_BOOK:
      const updatedBook: Book = action.payload.book
      return {
        ...state,
        books: state.books.map(book => {
          if (book.id === updatedBook.id) {
            book.description = updatedBook.description
          }
          return book;
        })
      }
    case ActionTypes.DELETE_BOOK:
      const deletedBook = action.payload.book
      return {
        ...state,
        books: [...state.books.filter(book => book.id !== deletedBook.id)]
      }

    case ActionTypes.ADD_COUNTRY:
      const addCountryBook: Book = action.payload.book
      return {
        ...state,
        books: [...state.books.map(book => {
          if (book.id === addCountryBook.id) {
            book.countries = [...book.countries, { name: "USA", icon: "🇺🇸"}]
            book.description = book.description + " and also american now."
          }
          return book;
        })]
      }

    // Async
    case ActionTypes.ASYNC_ACTION_START:
      return {
        ...state,
        loading: true,
        elementName: action.payload.elementName
      }

    case ActionTypes.ASYNC_ACTION_FINISH:
      return {
        ...state,
        loading: false,
        elementName: null
      }

    case ActionTypes.ASYNC_ACTION_ERROR:
      return {
        ...state,
        loading: false,
        elementName: null
      }

    // Fallback & Initial State (no-op)
    default:
      return state
  }
}

// Just faking an arbitrary delay
const delay = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms))
}


