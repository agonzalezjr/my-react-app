import React, { Component } from 'react'
import { Segment, Item, List, Button, Icon } from 'semantic-ui-react';
import { BookFeedFlag } from './BookFeedFlag';
import Book from '../../../data/Book';
import { NavLink } from 'react-router-dom';

// This component doesn't use a state, so class vs. function component doesn't give us much
// Would prefer "function", but leaving it as a "class" for an example

export default class BookFeedItem extends Component<{book: Book, selectBook: Function, deleteBook: Function}> {
  render() {

    // Wow :O de-structuring is cool!
    const { book, selectBook, deleteBook } = this.props;

    return (
      <Segment.Group>
        <Segment>
          <Item.Group>
            <Item>
              <Item.Image size="tiny" src="assets/red-book.png"></Item.Image>
              <Item.Content>
                <Item.Header>{book.title}</Item.Header>
                <Item.Description>{book.description}</Item.Description>
              </Item.Content>
            </Item>
          </Item.Group>
        </Segment>
        <Segment secondary>
          <Icon name="world"/>
          <List horizontal>
            {book.countries.map(country => (
              <BookFeedFlag key={country.name} country={country}/>
            ))}
          </List>
        </Segment>
        <Segment clearing>
          <Button onClick={() => {
            deleteBook(book)
          }} as="a" color="red" floated="right" content="Delete" />
          <Button 
            as={NavLink} 
            to={`/books/${book.id}`} 
            color="blue" 
            floated="right" 
            content="Open" />
          <Button
            onClick={() => selectBook(book)}
            color="teal" 
            floated="right" 
            content="Update"
          ></Button>
        </Segment>
      </Segment.Group>
    )
  }
}
