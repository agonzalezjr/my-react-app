import React, { Component, Fragment } from 'react'
import BookFeedItem from './BookFeedItem';
import Book from '../../../data/Book';

// An example of a component witha list. This one has no state either.

export default class BookFeedList extends Component<{books: Book[], selectBook: Function, deleteBook: Function}> {
  render() {
    const { books, selectBook, deleteBook } = this.props
    return (
      <Fragment>
        {books.map(book => (
          <BookFeedItem 
            key={book.id} 
            book={book} 
            selectBook={selectBook}
            deleteBook={deleteBook} />
        ))}
      </Fragment>
    )
  }
}
