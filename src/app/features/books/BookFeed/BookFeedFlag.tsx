import React, { FunctionComponent } from 'react'
import { Button } from 'semantic-ui-react';
import { ICountry } from '../../../data/Country';
 
// #andy: This is just another way of declaring a functional component
// the type is not contributing much here other than safety

type BookFeedFlagProps = {
  country: ICountry
}

// Note there is no "props" here
// De-structuring is automatic!!
export const BookFeedFlag: FunctionComponent<BookFeedFlagProps> = ({ country }) => {

  // return with the render right away
  return (
    <Button>{country.name} {country.icon}</Button>
  )
}
