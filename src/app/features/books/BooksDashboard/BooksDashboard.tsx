import React, { FunctionComponent, useState } from 'react'
import { Grid, Button } from 'semantic-ui-react';
import BookFeedList from '../BookFeed/BookFeedList';
import Book from '../../../data/Book';
// import BookForm from '../BookForm/BookForm';
import { BookFormHooked } from '../BookForm/BookFormHooked';
import { connect } from 'react-redux';
import { IAppState, Action, Actions } from '../../testarea/testReducer';

// Props
interface IBookDashBoardProps {
  books: Book[]
}

const mapStoreStateToProps = (state: IAppState): IBookDashBoardProps => ({
  books: state.books
})

// Actions
interface IBookDashBoardActionProps {
  createBook: Action,
  updateBook: Action,
  deleteBook: Action
}

const mapReducerActionsToProps: IBookDashBoardActionProps = Actions

const BooksDashboard: FunctionComponent<IBookDashBoardProps & IBookDashBoardActionProps> = ({ books, createBook, updateBook, deleteBook }) => {

  const [isFormOpen, setIsFormOpen] = useState(false)
  const [selectedBook, setSelectedBook] = useState<Book | null>(null)

  const openFormForDisplay = (book: Book) => {
    setIsFormOpen(true)
    setSelectedBook(book)
  }

  const openFormForCreate = () => {
    setIsFormOpen(true)
    setSelectedBook(null)
  }

  const cancelForm = () => {
    setIsFormOpen(false)
    setSelectedBook(null)
  }

  const createBookWrapper = (book: Book) => {
    cancelForm()
    createBook(book)
  }

  const deleteBookWrapper = (book: Book) => {
    if (selectedBook !== null && book.id === selectedBook.id) {
      cancelForm()
    }
    deleteBook(book)
  }

  return (
    <Grid>
      <Grid.Column width={10}>
        <BookFeedList books={books} selectBook={openFormForDisplay} deleteBook={deleteBookWrapper}/>
      </Grid.Column>
      <Grid.Column width={6}>
        {!isFormOpen && <Button positive content="Add Book" onClick={openFormForCreate}/>}

        {/* This is a trick! By using a differet key each time, the component is guaranteed to be re-rendered */}

        {isFormOpen && <BookFormHooked 
          key={selectedBook ? selectedBook.title : 0} 
          selectedBook={selectedBook}
          cancel={cancelForm} 
          create={createBookWrapper} 
          update={updateBook}
        />}
      </Grid.Column>
    </Grid>
  )
}

export default connect(mapStoreStateToProps, mapReducerActionsToProps)(BooksDashboard)
