import React, { FunctionComponent } from 'react'
import Book from '../../../data/Book'
import { useDispatch, useSelector } from 'react-redux';
import { Actions, IAppState } from '../../testarea/testReducer';
import { RouteComponentProps } from 'react-router';
import { Button } from 'semantic-ui-react';

// All of this is gone with react-redux hooks!!

// type BookDetailsPageProps = {
//   bookId: number,
//   book: Book
// }

// const mapState = (state: IAppState, ownProps: RouteComponentProps<{ id: string }>): BookDetailsPageProps => {
//   const bookId = Number(ownProps.match.params.id)
//   let book = new Book(-1, "BAD", "VERY BAD", [])
//   if (bookId && state.books.length > 0) {
//     book = state.books.filter(book => book.id === bookId)[0]
//   }
//   return {
//     bookId,
//     book
//   }
// }

// type BookDetailsPageActions = {
//   addCountry: Action
// }

// const mapActions: BookDetailsPageActions = Actions

const BookDetailsPage: FunctionComponent<RouteComponentProps<{id: string}>> = (ownProps) => {

  // react-redux-hook: Using this, we can call actions to our heart's content
  const { addCountry } = Actions
  const dispatch = useDispatch()

  // react-redux-hooks: Using this, we can get access properties automatically
  const books = useSelector((state: IAppState) => state.books)
  
  // Filter down to the right book
  const bookId = Number(ownProps.match.params.id)
  const book = (books as Book[]).filter(aBook => aBook.id === bookId)[0]

  const onAddCountry = () => {
    dispatch(addCountry(book))
  }

  return (
    <div>
      <h1>{book.title}</h1>
      <h3>{book.description}</h3>
      <h4>(ID = {book.id})</h4>
      <h2>Countries</h2>
      <ul>
        {book.countries.map(country => {
          return <li key={country.name}>{country.name} {country.icon}</li>
        })}
      </ul>
      <h2>Add a Country (to test actions)</h2>
      <Button 
        content="Add USA 🇺🇸" 
        onClick={onAddCountry}>
      </Button>
    </div>
  )
}

// No need to connect anymore
// export default connect(mapState, mapActions)(BookDetailsPage)
export default BookDetailsPage
