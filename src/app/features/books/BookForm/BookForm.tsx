import React, { Component, SyntheticEvent } from 'react'
import { Segment, Form, Button } from 'semantic-ui-react';
import Book from '../../../data/Book';

type BookFormProps = {
  cancel: () => void,
  create: (book: Book) => void,
  update: (book: Book) => void,
  selectedBook: Book | null
}

type BookFormState = {
  title: string,
  description: string
}

export default class BookForm extends Component<BookFormProps, BookFormState> {

  constructor(props: BookFormProps) {
    super(props);

    this.state = {
      title: "",
      description: ""
    }
  }

  componentDidMount() {
    console.log(`> Form's componentDidMount`)
    if (this.props.selectedBook !== null) {
      this.setState({
        title: this.props.selectedBook.title,
        description: this.props.selectedBook.description
      })
    }
  }

  // These have to be declared as properties so
  // that binding to 'this' happens automatically!
  
  onFormSubmit = (evt: SyntheticEvent) => {
    const { create, update, selectedBook } = this.props
    if (selectedBook) {

      // We could do something like this here, but that would be bad!! selectedBook
      // is a "prop", not state, and we can't change those!! Pass the change back
      // up to the component that owns the "prop" as a "state" and let it do the change!!!
      // selectedBook.description = this.state.description
      // update()

      const updatedBook = new Book(0, selectedBook.title, this.state.description, [])
      update(updatedBook)
    } else {
      const newBook = new Book(0, this.state.title, this.state.description, [])
      create(newBook)
    }
  };

  // Fix this! I am sure with hooks it will be much better!!
  onInputChange = (evt: any) => {
    if (evt.target.name === 'title') {
      this.setState({
        title: evt.target.value as string
      })
    } else {
      this.setState({
        description: evt.target.value as string
      })
    }
  }

  handleCancel = () => {
    this.props.cancel()
  }

  render() {
    const { selectedBook } = this.props
    const { title, description } = this.state

    return (
      <Segment>
        <Form onSubmit={this.onFormSubmit} autoComplete="off">
          <Form.Field>
            <label>Title</label>
            <input disabled={selectedBook !== null} name='title' onChange={this.onInputChange} value={title} placeholder="Book Title" />
          </Form.Field>
          <Form.Field>
            <label>Description</label>
            <textarea name='description' onChange={this.onInputChange} value={description} placeholder="Book Description" />
          </Form.Field>
          {/* #andytodo: add a dropdown of countries */}
          {/* <Form.Field>
            <label>Countries</label>
            <input name='countries' onChange={this.onInputChange} type="dropdown" />
          </Form.Field> */}
          <Button positive type="submit">
            {selectedBook ? "Update" : "Create"}
          </Button>
          <Button onClick={this.handleCancel} type="button">
            Cancel
          </Button>
        </Form>
      </Segment>
    )
  }
}
