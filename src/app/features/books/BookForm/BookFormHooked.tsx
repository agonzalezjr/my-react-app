import React, { FunctionComponent, useState, useEffect } from 'react'
import { Segment, Form, Button } from 'semantic-ui-react';
import Book from '../../../data/Book';

type BookFormProps = {
  cancel: () => void,
  create: (book: Book) => void,
  update: (book: Book) => void,
  selectedBook: Book | null
}

// type BookFormState = {
//   title: string,
//   description: string
// }

// If using hooks, need to be a "function" component
export const BookFormHooked: FunctionComponent<BookFormProps> = (props) => {

  // This is how the state is declared and init'ed with hooks. Props are used for the initial state.
  // const [title, setTitle] = useState(props.selectedBook ? props.selectedBook.title : "")
  // const [description, setDescription] = useState(props.selectedBook ? props.selectedBook.description : "")

  // Initial state can also be handled using useEffect()
  const [title, setTitle] = useState("")
  useEffect(() => {
    if (props.selectedBook) {
      setTitle(props.selectedBook.title)
    }
  }, [props.selectedBook])

  const [description, setDescription] = useState("")
  useEffect(() => {
    if (props.selectedBook) {
      setDescription(props.selectedBook.description)
    }
  }, [props.selectedBook])  

  // Can't use this in a function component!
  // componentDidMount() { ... }
  // use "useEffect()" instead

  const onFormSubmit = () => {
    const { create, update, selectedBook } = props
    if (selectedBook) {
      const updatedBook = new Book(selectedBook.id, selectedBook.title, description, selectedBook.countries)
      update(updatedBook)
    } else {
      const newBook = new Book(1000, title, description, [])
      create(newBook)
    }
  }

  const { selectedBook } = props

  return (
    <Segment>
      <Form onSubmit={onFormSubmit} autoComplete="off">
        <Form.Field>
          <label>Title</label>
          <input 
            value={title} 
            disabled={selectedBook !== null} 
            onChange={(evt) => { 
              setTitle(evt.target.value as string) 
            }} 
            placeholder="Book Title" 
          />
        </Form.Field>
        <Form.Field>
          <label>Description</label>
          <textarea 
            value={description} 
            onChange={(evt) => {
              setDescription(evt.target.value as string)
            }} 
            placeholder="Book Description" 
          />
        </Form.Field>
        {/* #andytodo: add a dropdown of countries */}
        {/* <Form.Field>
          <label>Countries</label>
          <input name='countries' onChange={this.onInputChange} type="dropdown" />
        </Form.Field> */}
        <Button positive type="submit">
          {selectedBook ? "Update" : "Create"}
        </Button>
        <Button onClick={props.cancel} type="button">
          Cancel
        </Button>
      </Form>
    </Segment>
  );
}
