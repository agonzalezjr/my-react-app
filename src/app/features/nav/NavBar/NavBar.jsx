import React, { Component } from 'react';
import { Menu, Container, Button } from 'semantic-ui-react';
import { NavLink, Link, withRouter } from 'react-router-dom';

class NavBar extends Component {
  render() {
    return (
      <Menu inverted fixed="top">
        <Container>
          <Menu.Item as={NavLink} exact to="/" header>
            <img src="assets/open-book.png" alt="logo" />
            Atlas
          </Menu.Item>

          {/* The difference between NavLink and Link is the highlight of the UI (done by CSS) */}

          <Menu.Item exact as={NavLink} to="/books" name="Books" />
          <Menu.Item as={NavLink} to="/countries" name="Countries" />
          <Menu.Item>
            <Button as={Link} to="/createBook" floated="right" positive inverted content="Add Book" />
          </Menu.Item>
          <Menu.Item position="right">
            <Button disabled basic inverted content="Login" />
            <Button disabled basic inverted content="Sign Out" style={{ marginLeft: '0.5em' }} />
          </Menu.Item>
          <Menu.Item as={NavLink} to="/test" name="Test 🧪" />
        </Container>
      </Menu>
    );
  }
}

export default withRouter(NavBar);
