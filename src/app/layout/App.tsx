import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';
import NavBar from '../features/nav/NavBar/NavBar';
import BooksDashboard from '../features/books/BooksDashboard/BooksDashboard';
import { BrowserRouter as Router, Route } from "react-router-dom"
import HomePage from "../features/home/HomePage"
import CountriesDashboard from '../features/country/CountriesDashboard/CountriesDashboard';
import BookDetailsPage from '../features/books/BookDetails/BookDetailsPage';
import CountryDetailsPage from '../features/country/CountryDetails/CountryDetailsPage';
import { BookFormHooked } from '../features/books/BookForm/BookFormHooked';
import TestComponent from '../features/testarea/TestComponent';
import { Provider } from 'react-redux';
import { configureStore } from '../store/configureStore';
import ScrollToTop from '../util/ScrollToTop';

const store = configureStore()

console.log(`> store's state = ${JSON.stringify(store.getState())}`)
class App extends Component {
  
  render() {
    return (
      <Provider store={store}>
        <Router>
          <ScrollToTop>
          <NavBar />
            <Container className="main">
                <Route exact path="/" component={HomePage} />
                <Route exact path="/books" component={BooksDashboard} />
                <Route path="/books/:id" component={BookDetailsPage} />
                <Route path="/createBook" component={BookFormHooked} />
                <Route path="/countries" component={CountriesDashboard} />
                <Route path="/countries/:id" component={CountryDetailsPage} />
                <Route path="/test" component={TestComponent} />
            </Container>
          </ScrollToTop>
        </Router>
      </Provider>
    );
  }
}

export default App;
